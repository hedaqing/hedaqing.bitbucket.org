create view student_info(activity,
    admission_photo,serial_number, award_information,
    birth_date, contact_email,
    contact_mobile, current_high_school_address, current_high_school_class_duty,
    current_high_school_class,
    current_high_school_name, english_name, enroll_address, enroll_num,
    father_education_background, father_mobile, father_name, father_title, father_work_duty, father_work_unit,
    gender, high_school_head_teacher_mobile, high_school_head_teacher_name, high_school_roll, home_address,
    id_card_num, id, leadership_mobile, leadership_name, mother_education_background, mother_mobile,
    mother_name, mother_title, mother_work_duty, mother_work_unit,
    name, senior_one_info, senior_three_info, senior_two_info, state,
    exam_subject, foreign_language, graduate_type,
    nation, political_status, student_type, subject_type,
    apply_major1, apply_major2, apply_major3, current_high_school_level, shiro_user_id )
AS select
     atv.name,
     afi.image, af.serial_number, af.award_information,
     af.birth_date, af.contact_email,
     af.contact_mobile, af.current_high_school_address, af.current_high_school_class_duty,
     af.current_high_school_class,
     af.current_high_school_name, af.english_name, af.enroll_address, af.enroll_num,
     af.father_education_background, af.father_mobile, af.father_name, af.father_title, af.father_work_duty, af.father_work_unit,
     af.gender, af.high_school_head_teacher_mobile, af.high_school_head_teacher_name, af.high_school_roll, af.home_address,
     af.id_card_num, af.id, af.leadership_mobile, af.leadership_name, af.mother_education_background, af.mother_mobile,
     af.mother_name, af.mother_title, af.mother_work_duty, af.mother_work_unit,
     af.name, af.senior_one_info, af.senior_three_info, af.senior_two_info, af.state,
     dict_1.name AS exam_subject,
     dict_2.name AS foreign_language,
     dict_3.name AS graduate_type,
     dict_4.name AS nation,
     dict_5.name AS political_status,
     dict_6.name AS student_type,
     dict_7.name AS subject_type,
     dict_8.name AS apply_major1,
     dict_9.name AS apply_major2,
     dict_10.name AS apply_major3,
     dict_11.name AS current_high_school_level,
     shiro_user.id AS user_id
   from
     admission_form af
     LEFT JOIN admission_activity atv ON af.activity_id = atv.id
     LEFT JOIN admission_form_image afi ON af.id = afi.form_id
     LEFT JOIN base_common_dict dict_1 ON af.exam_subject_id = dict_1.id
     LEFT JOIN base_common_dict dict_2 ON af.foreign_language_id = dict_2.id
     LEFT JOIN base_common_dict dict_3 ON af.graduate_type_id = dict_3.id
     LEFT JOIN base_common_dict dict_4 ON af.nation_id = dict_4.id
     LEFT JOIN base_common_dict dict_5 ON af.political_status_id = dict_5.id
     LEFT JOIN base_common_dict dict_6 ON af.student_type_id = dict_6.id
     LEFT JOIN base_common_dict dict_7 ON af.subject_type_id = dict_7.id
     LEFT JOIN base_common_dict dict_8 ON af.apply_major1_id = dict_8.id
     LEFT JOIN base_common_dict dict_9 ON af.apply_major2_id = dict_9.id
     LEFT JOIN base_common_dict dict_10 ON af.apply_major3_id = dict_10.id
     LEFT JOIN base_common_dict dict_11 ON af.current_high_school_level_id = dict_11.id
     LEFT JOIN shiro_user ON af.user_id = shiro_user.id;
