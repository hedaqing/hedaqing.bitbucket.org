# coding=utf-8

import os,sys
import subprocess,cmd

iplist = open('ip_list')
ip_True = open('ip_true','w+')
ip_False = open('ip_false','w+')
count_True,count_False = 0,0
for line in iplist:
    ip=line.strip()
    if not ip: 
        continue
    res = subprocess.call("ping -c 2 -w 5 %s" % ip,shell=True,stdout=subprocess.PIPE)
    if res:
        print 'ping %s is fail'%ip
        ip_False.write(ip+'\n')
        count_False += 1
    else:
        print 'ping %s is ok'%ip
        ip_True.write(ip+'\n')
        count_True += 1
ip_True.close()
ip_False.close()
iplist.close()
print "ping通的ip数：",count_True,"   ping不通的ip数：",count_False
