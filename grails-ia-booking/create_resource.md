# 创建新的资源

创建新的资源
1. 选择资源类型：首先选择资源类型，根据资源类型生成页面内容
2. 选择资源组：筛选当前用户现有的分组，如果没有分组则创建默认组，如果有，则选择现有分组。
   *如果现有分组中没有需要的组，则需要首先创建一个新的分组，再创建该资源*
3. 填写资源相关信息，选择时间模式。根据选择的时间模式生成时间选择界面。
4. 指定查看权限和借用权限
5. 如果有权限限制，则添加可以使用的用户


# 查看资源

1. 根据组的拥有者，筛选当前用户拥有的资源
2. 根据资源的权限和组的权限，筛选其余的资源有哪些可以查看
3. 可以借用的资源，显示借用按钮

# time mode 
1. 三时段
3
上午(8:00-12:00),下午(12:00-18:00),晚上(18:00-22:00)
2. 按小时划分
16
0:00-1:00,1:00-2:00,2:00-3:00,3:00-4:00,4:00-5:00,6:00-7:00,7:00-8:00,8:00-9:00,9:00-10:00,10:00-11:00,11:00-12:00,12:00-13:00,13:00-14:00,14:00-15:00,15:00-16:00,16:00-17:00,17:00-18:00,18:00-19:00,19:00-20:00,20:00-21:00,21:00-22:00,22:00-23:00,23:00-0:00
3. 按节次划分
第一小节(8:00-8:45),第二小节(8:50-9-35),第三小节(9:50-10:35),第四小节(10:40-11:25),第五小节(11:30-12:15),第六小节(13:20-14:05),第七小节(14:10-14:55),第八小节(15:10-15:55),第九小节(16:00-16:45),第十小节(16:50-17:35),第十一小节(18:30-19:15),第十二小节(19:20-20:05),第十三小节(20:10-20:55)
# 资源换组

1. 检查组权限
2. 将资源的内容按照组的设置进行更新
3. 
