package com.my.db;

public class Mdl_programming_tests {
  private Long id;
  private Long programmingid;
  private Long seq;
  private String input;
  private String gzinput;
  private String output;
  private String gzoutput;
  private String cmdargs;
  private Long timelimit;
  private Long memlimit;
  private Long nproc;
  private Long pub;
  private Long weight;
  private String memo;
  private Long timemodified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getProgrammingid() {
    return programmingid;
  }

  public void setProgrammingid(Long programmingid) {
    this.programmingid = programmingid;
  }

  public Long getSeq() {
    return seq;
  }

  public void setSeq(Long seq) {
    this.seq = seq;
  }

  public String getInput() {
    return input;
  }

  public void setInput(String input) {
    this.input = input;
  }

  public String getGzinput() {
    return gzinput;
  }

  public void setGzinput(String gzinput) {
    this.gzinput = gzinput;
  }

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }

  public String getGzoutput() {
    return gzoutput;
  }

  public void setGzoutput(String gzoutput) {
    this.gzoutput = gzoutput;
  }

  public String getCmdargs() {
    return cmdargs;
  }

  public void setCmdargs(String cmdargs) {
    this.cmdargs = cmdargs;
  }

  public Long getTimelimit() {
    return timelimit;
  }

  public void setTimelimit(Long timelimit) {
    this.timelimit = timelimit;
  }

  public Long getMemlimit() {
    return memlimit;
  }

  public void setMemlimit(Long memlimit) {
    this.memlimit = memlimit;
  }

  public Long getNproc() {
    return nproc;
  }

  public void setNproc(Long nproc) {
    this.nproc = nproc;
  }

  public Long getPub() {
    return pub;
  }

  public void setPub(Long pub) {
    this.pub = pub;
  }

  public Long getWeight() {
    return weight;
  }

  public void setWeight(Long weight) {
    this.weight = weight;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Long getTimemodified() {
    return timemodified;
  }

  public void setTimemodified(Long timemodified) {
    this.timemodified = timemodified;
  }
}
