package com.my.db;

public class Mdl_programming {
  private Long id;
  private Long course;
  private String name;
  private String intro;
  private Long introformat;
  private String grade;
  private String globalid;
  private Long timeopen;
  private Long timeclose;
  private Long timelimit;
  private Long memlimit;
  private Long nproc;
  private Long timediscount;
  private Long discount;
  private Long allowlate;
  private Long attempts;
  private Long keeplatestonly;
  private String inputfile;
  private String outputfile;
  private String presetcode;
  private String generator;
  private String validator;
  private Long generatortype;
  private Long validatortype;
  private Long validatorlang;
  private Long showmode;
  private Long timemodified;
  private Long completionac;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCourse() {
    return course;
  }

  public void setCourse(Long course) {
    this.course = course;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public Long getIntroformat() {
    return introformat;
  }

  public void setIntroformat(Long introformat) {
    this.introformat = introformat;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getGlobalid() {
    return globalid;
  }

  public void setGlobalid(String globalid) {
    this.globalid = globalid;
  }

  public Long getTimeopen() {
    return timeopen;
  }

  public void setTimeopen(Long timeopen) {
    this.timeopen = timeopen;
  }

  public Long getTimeclose() {
    return timeclose;
  }

  public void setTimeclose(Long timeclose) {
    this.timeclose = timeclose;
  }

  public Long getTimelimit() {
    return timelimit;
  }

  public void setTimelimit(Long timelimit) {
    this.timelimit = timelimit;
  }

  public Long getMemlimit() {
    return memlimit;
  }

  public void setMemlimit(Long memlimit) {
    this.memlimit = memlimit;
  }

  public Long getNproc() {
    return nproc;
  }

  public void setNproc(Long nproc) {
    this.nproc = nproc;
  }

  public Long getTimediscount() {
    return timediscount;
  }

  public void setTimediscount(Long timediscount) {
    this.timediscount = timediscount;
  }

  public Long getDiscount() {
    return discount;
  }

  public void setDiscount(Long discount) {
    this.discount = discount;
  }

  public Long getAllowlate() {
    return allowlate;
  }

  public void setAllowlate(Long allowlate) {
    this.allowlate = allowlate;
  }

  public Long getAttempts() {
    return attempts;
  }

  public void setAttempts(Long attempts) {
    this.attempts = attempts;
  }

  public Long getKeeplatestonly() {
    return keeplatestonly;
  }

  public void setKeeplatestonly(Long keeplatestonly) {
    this.keeplatestonly = keeplatestonly;
  }

  public String getInputfile() {
    return inputfile;
  }

  public void setInputfile(String inputfile) {
    this.inputfile = inputfile;
  }

  public String getOutputfile() {
    return outputfile;
  }

  public void setOutputfile(String outputfile) {
    this.outputfile = outputfile;
  }

  public String getPresetcode() {
    return presetcode;
  }

  public void setPresetcode(String presetcode) {
    this.presetcode = presetcode;
  }

  public String getGenerator() {
    return generator;
  }

  public void setGenerator(String generator) {
    this.generator = generator;
  }

  public String getValidator() {
    return validator;
  }

  public void setValidator(String validator) {
    this.validator = validator;
  }

  public Long getGeneratortype() {
    return generatortype;
  }

  public void setGeneratortype(Long generatortype) {
    this.generatortype = generatortype;
  }

  public Long getValidatortype() {
    return validatortype;
  }

  public void setValidatortype(Long validatortype) {
    this.validatortype = validatortype;
  }

  public Long getValidatorlang() {
    return validatorlang;
  }

  public void setValidatorlang(Long validatorlang) {
    this.validatorlang = validatorlang;
  }

  public Long getShowmode() {
    return showmode;
  }

  public void setShowmode(Long showmode) {
    this.showmode = showmode;
  }

  public Long getTimemodified() {
    return timemodified;
  }

  public void setTimemodified(Long timemodified) {
    this.timemodified = timemodified;
  }

  public Long getCompletionac() {
    return completionac;
  }

  public void setCompletionac(Long completionac) {
    this.completionac = completionac;
  }
}
