package com.my.db;

public class Mdl_programming_presetcode {
  private Long id;
  private Long programmingid;
  private Long languageid;
  private String name;
  private Long sequence;
  private String presetcode;
  private String presetcodeforcheck;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getProgrammingid() {
    return programmingid;
  }

  public void setProgrammingid(Long programmingid) {
    this.programmingid = programmingid;
  }

  public Long getLanguageid() {
    return languageid;
  }

  public void setLanguageid(Long languageid) {
    this.languageid = languageid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSequence() {
    return sequence;
  }

  public void setSequence(Long sequence) {
    this.sequence = sequence;
  }

  public String getPresetcode() {
    return presetcode;
  }

  public void setPresetcode(String presetcode) {
    this.presetcode = presetcode;
  }

  public String getPresetcodeforcheck() {
    return presetcodeforcheck;
  }

  public void setPresetcodeforcheck(String presetcodeforcheck) {
    this.presetcodeforcheck = presetcodeforcheck;
  }
}
