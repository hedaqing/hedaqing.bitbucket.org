package com.my.db;

public class Mdl_programming_datafile {
  private Long id;
  private Long programmingid;
  private Long seq;
  private String filename;
  private Long isbinary;
  private Long datasize;
  private String data;
  private Long checkdatasize;
  private String checkdata;
  private String memo;
  private Long timemodified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getProgrammingid() {
    return programmingid;
  }

  public void setProgrammingid(Long programmingid) {
    this.programmingid = programmingid;
  }

  public Long getSeq() {
    return seq;
  }

  public void setSeq(Long seq) {
    this.seq = seq;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public Long getIsbinary() {
    return isbinary;
  }

  public void setIsbinary(Long isbinary) {
    this.isbinary = isbinary;
  }

  public Long getDatasize() {
    return datasize;
  }

  public void setDatasize(Long datasize) {
    this.datasize = datasize;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Long getCheckdatasize() {
    return checkdatasize;
  }

  public void setCheckdatasize(Long checkdatasize) {
    this.checkdatasize = checkdatasize;
  }

  public String getCheckdata() {
    return checkdata;
  }

  public void setCheckdata(String checkdata) {
    this.checkdata = checkdata;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Long getTimemodified() {
    return timemodified;
  }

  public void setTimemodified(Long timemodified) {
    this.timemodified = timemodified;
  }
}
