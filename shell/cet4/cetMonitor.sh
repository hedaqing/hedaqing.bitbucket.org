#!/bin/bash

if ["$1" = ""]; then
    echo "请输入文件名称\t\n"
    exit 1
fi

list=$1

if [ ! -f "$list" ]; then 
    echo "文件不存在\t\n"
    exit 1
fi

log=ping.log

#清空日志
echo -n "" > $log

#从$list文件中读出ip地址列表，去掉包含#的行和空行，结果写入 .list
grep -v "#" $list | grep -v "^$" > .list

#从.list读入ip地址，执行ping检测

OLD_IFS="$IFS" 
IFS="," 
DATE=$(date +%Y%m%d)
while read IP
do
    li=($IP)
    #echo ${li[0]}
    ping -c 2  ${li[1]} > /dev/null && echo "${li[0]} PASS" || echo "${li[0]} LOSS IP:${li[2]}  $DATE" >> $log
    if test ${li[2]}; then
        ping -c 2  ${li[2]} > /dev/null && echo "${li[0]} PASS" || echo "${li[0]} LOSS IP:${li[1]}  $DATE" >> $log
    fi
#    echo "`ping -s 1000 -f -c 100  10.0.1.1 | grep transmitted | awk '{print $6}'`" >> $log
done < .list
grep -v " 0%" $log
rm .list
IFS="$OLD_IFS"

